// import logo from './logo.svg';
import React, { Fragment } from 'react';
import { HashRouter as Router, BrowserRouter,  Routes, Route } from 'react-router-dom';
import Home from './pages/Home/index'
import 'upkit/dist/style.min.css';
import { Provider } from 'react-redux';
import store from './app/store';
import { listen } from './app/listener';
import Register from './pages/Register';
import RegisterSuccess from './pages/Register/RegisterSuccess';
import Login from './pages/Login';

function App() {
  React.useEffect(() => {
    listen();
  }, []);
  return (
    <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path="/register" element={<Register />} />
        <Route path="/" element={<Home />} />
        <Route path="/register/berhasil" element={<RegisterSuccess />} />
        <Route path="/login" element={<Login />}></Route>
      </Routes>
      </BrowserRouter>
      </Provider>
  );
}

export default App;
