import * as React from 'react';
import { LayoutOne, Card, FormControl, InputText, InputPassword, Button } from 'upkit';
import { useForm } from 'react-hook-form';
import { rules } from './validation';
import { registerUser } from '../../api/auth';
import { Link, useNavigate } from 'react-router-dom';
import StorageLogo from '../../components/StoreLogo';

const  statuslist = {
 idle: 'idle', 
 process: 'process', 
 success: 'success', 
 error: 'error',
}


export default function Register() {
    let { register, handleSubmit, formState : { errors }, setError } = useForm();
    let [status, setStatus] = React.useState(statuslist.idle);
    let navigate = useNavigate();

    const onSubmit = async (formData)=> {
        console.log(formData);
       
        let { password, password_confirmation } = formData;

        if (password !== password_confirmation) {
            return setError('password_confirmation', {
                type: 'equality', message: 'Konfirmasi password harus dama dengan password'
            });
        }
        setStatus(statuslist.process);
        let { data } = await registerUser(formData);
        
        if (data.error) {
            let fields = Object.keys(data.fields);
            fields.forEach(field => {
                    setError(field, {type: 'server', message:
                    data.fields[field]?.properties?.message}) 
            });
        }

        setStatus(statuslist.success);
        navigate('/register/berhasil');
    };
    return (
        <LayoutOne size='small'>
            <Card color='white'>
                <div className='text-center mb-5'>
                    <StorageLogo />
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl>
                        <InputText
                            name="full_name" placeholder="nama lengkap" fitContainer
                            {...register("full_name",  rules.full_name)}
                        />
                        {errors.full_name && `${errors.full_name?.message}`}
                    </FormControl>
                    <FormControl>
                        <InputText name="email" placeholder="Input Email" fitContainer {...register('email', rules.email)} />
                        {errors.email && `${errors.email?.message}`}
                    </FormControl>
                    <FormControl>
                        <InputPassword name="password" placeholder='password' fitContainer {...register('password', rules.password)} />
                        {errors.password && `${errors.password?.message}`}
                    </FormControl>
                    <FormControl>
                        <InputPassword name="password_confirmation" placeholder='konfirmasi password' fitContainer {...register('password_confirmation', rules.password_confirmation)} />
                        {errors.password_confirmation && `${errors.password_confirmation?.message}`}
                    </FormControl>
                    <Button size='large' fitContainer>Mendaftar</Button>
                </form>

                <div className='text-center mt-2'>
                    Sudah punya akun? <Link to="/login"><b>Masuk Sekrang</b></Link>
                    </div>
            </Card>
        </LayoutOne>
    );
}