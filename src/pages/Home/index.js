import * as React from 'react'; 
import { useDispatch, useSelector } from 'react-redux';
import { SideNav, LayoutSidebar, Responsive, CardProduct, Pagination } from 'upkit';
import menus from './menus';
import TopBar from '../../components/TopBar';
import { config } from '../../config';
import { fetchProducts } from '../../features/Products/action';
import { setPage,goToPrevPage, goToNextPage  } from '../../features/Products/action';

export default function Home() {
    let dispatch = useDispatch();
    let products = useSelector(state => state.porducts);
    
    React.useEffect(() => {
        dispatch(fetchProducts())
    }, [dispatch, products.currentPage]);

    return (
     <div>
         <LayoutSidebar sidebar={<SideNav items={menus} verticalAlign='top' />}
             content={
             <div className='md:flex w-full mr-5 h-full min-h-screen'>
                 <div className="w-full md:w-3/4 pl-5 pb-10">
                         <TopBar />

                         <Responsive >
                        {products?.data?.map((product, index) => {
                        return <div key={index} className="p-2">
                        <CardProduct
                        title={product.name}
                        imgUrl=
                            {`${config.api_host}/upload/${product.image_url}`}
                              price={product.price}
                               onAddToCart={_ => null}
                        />
                        </div>
                        })}
                             <div className='text-center my-10'>
                                 <Pagination
                                     totalItems={products.totalItems}
                                        page={products.currentPage}
                                        perPage={products.perPage}
                                        onChange={page => dispatch(setPage(page))}
                                        onNext={_ => dispatch(goToNextPage())}
                                        onPrev={_ => dispatch(goToPrevPage())}
                                 />
                             </div>
                         </Responsive>
                         

                </div>
                 <div className="w-full md:w-1/4 h-full shadow-lg border-r border-white bg-gray-100">
                       Keranjang belanja di sini
                </div>
             </div>
         } sidebarSize={80}/>
    </div>
 )
}