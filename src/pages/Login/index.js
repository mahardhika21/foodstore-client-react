import * as React from 'react';
import { validation } from './validation';
import { InputText, InputPassword, Button, FormControl, Card, LayoutOne } from 'upkit';
import { useForm } from 'react-hook-form'
import { useNavigate, Link} from 'react-router-dom';
import StorageLogo from '../../components/StoreLogo';
import { useDispatch, useSelector } from 'react-redux';
import { userLogin } from '../../features/Auth/action';
import { rules } from './validation';
import { login } from '../../api/auth';

const statusList = {
    idle: 'idle',
    process: 'process',
    success: 'success',
    error : 'error'
};


export default function Login() {
    const { register, handleSubmit, errors, setError } = useForm();
    const [status, setStatus] = React.useState(statusList.idle);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onSubmit = async (email, password) => {
        let { data } = await login(email, password);

        if (data.error) {
            setError('password', { type: 'invalidCredential', message: data.message });
            setStatus(statusList.error);
        } else {
            let { user, token } = data;

            dispatch(userLogin(user, token));
            navigate('/');
        }

        setStatus(statusList.success);
    }

    return (
        <LayoutOne size='small'>
            <br />
            <div className='white'>
                <div className='text-center mb-5'>
                    <StorageLogo />
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl>
                        <InputText placeholder='email'
                            fitContainer
                            name='email'
                            {...register('email',rules.email)}
                        />
                    </FormControl>
                    <FormControl>
                        <InputText placeholder='password' fitContainer
                        name="password" {...register('password', rules.password)}
                        />
                    </FormControl>
                    <Button fitContainer size='large' disabled={status === 'process'}>Login</Button>
                </form>
            </div>
        </LayoutOne>
    );
}