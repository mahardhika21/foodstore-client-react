const rules = {
    email: {
        required: { value: true, message: 'Email tidak boleh kosong' },
        maxLength : { value : 255, message : 'Panjang email maximal 255' }
    },
    password: {
        required: { value: true, message: 'password tidak boleh dikosongkan' },
        maxLength : {value : 255, message : 'Panjang password maksimal'},
    }
}

export {
    rules
}